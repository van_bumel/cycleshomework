//
//  main.m
//  CyclesHomeWork
//
//  Created by Chaban Nikolay on 10/27/15.
//  Copyright © 2015 Gleb Cherkashyn. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    int i;
    
    for ( i = 99 ; i > 0; i -= 3 ) {
        
        NSLog(@"%d",i);
        
        if (i % 5 == 0) {
            
            NSLog(@"Found one!");
           
            continue;
        }
    }
    return 0;
}
